import haxe.ds.StringMap;

class SpriteData {
	static public var data = [
		"bg" => [301, 301],
		"life" => [19, 23],
		"mLevel" => [154, 107],
		"partSlot" => [10, 11],
		"slot" => [30, 30],
		"timeLeft" => [80, 8],
		"timeLine" => [80, 8],
		"warning" => [301, 301],
	];
	static public var anchor:StringMap<Array<Int>> = new StringMap();
}
